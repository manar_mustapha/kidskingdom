package com.rxmuhammadyoussef.core.widget.bottomsheet;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.design.widget.BottomSheetDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.rxmuhammadyoussef.core.R;
import com.rxmuhammadyoussef.core.util.Preconditions;

import javax.annotation.Nonnull;

public class BottomSheetBuilder {

    private final View view;
    private BottomSheetDialog dialog;

    public BottomSheetBuilder(Context context, @LayoutRes int layoutRes) {
        Preconditions.checkNonNull(context, "context must be non null");
        view = LayoutInflater.from(context).inflate(layoutRes, null);
        dialog = new BottomSheetDialog(context);
        dialog.setCancelable(false);
        Window window = dialog.getWindow();
        if (window != null) {
            WindowManager.LayoutParams attributes = window.getAttributes();
            attributes.gravity = Gravity.BOTTOM;
            attributes.width = WindowManager.LayoutParams.MATCH_PARENT;
            attributes.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(attributes);
        }
        dialog.setContentView(view);
    }

    public BottomSheetBuilder clickListener(@IdRes int viewId, @Nonnull OnClickListener onClickListener) {
        view.findViewById(viewId).setOnClickListener(v -> onClickListener.onClick(dialog, v));
        return this;
    }

    public BottomSheetBuilder transparentBackground(boolean transparent) {
        if (transparent) {
            dialog.setOnShowListener(dialog -> {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = d.findViewById(R.id.design_bottom_sheet);
                if (bottomSheet == null) {
                    return;
                }
                bottomSheet.setBackground(null);
            });
        }
        return this;
    }

    public BottomSheetBuilder background(@DrawableRes int drawableRes) {
        Window window = dialog.getWindow();
        if (window != null) {
            window.getAttributes();
            window.setBackgroundDrawableResource(drawableRes);
        }
        return this;
    }

    public BottomSheetBuilder text(@IdRes int viewId, String text) {
        View view = this.view.findViewById(viewId);
        view.setVisibility(View.VISIBLE);
        ((TextView) view).setText(text);
        return this;
    }

    public BottomSheetBuilder cancelable(boolean cancelable) {
        dialog.setCancelable(cancelable);
        return this;
    }

    public Dialog build() {
        return dialog;
    }

    public interface OnClickListener {

        void onClick(BottomSheetDialog dialog, View view);
    }
}
