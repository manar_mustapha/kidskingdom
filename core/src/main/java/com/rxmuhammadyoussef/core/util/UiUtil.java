package com.rxmuhammadyoussef.core.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rxmuhammadyoussef.core.R;
import com.rxmuhammadyoussef.core.widget.bottomsheet.BottomSheetBuilder;
import com.rxmuhammadyoussef.core.widget.dialog.DialogBuilder;
import com.rxmuhammadyoussef.core.widget.snackbar.SnackBarBuilder;

import javax.inject.Inject;

public class UiUtil {

    private final Context context;
    private ProgressDialog progressDialog;

    @Inject
    public UiUtil(Context context) {
        Preconditions.checkNonNull(context, "should not pass null context reference");
        this.context = context;
    }

    public SnackBarBuilder getSnackBarBuilder(Context context, View containerLayout) {
        return new SnackBarBuilder(context, containerLayout);
    }

    public DialogBuilder getDialogBuilder(Context context, @LayoutRes int layout) {
        return new DialogBuilder(context, layout);
    }

    public BottomSheetBuilder getBottomSheetBuilder(Context context, @LayoutRes int layout) {
        return new BottomSheetBuilder(context, layout);
    }

    private LayoutInflater getLayoutInflater() {
        return LayoutInflater.from(context);
    }

    public ProgressDialog getProgressDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(context);
        }
        progressDialog.setCancelable(false);
        progressDialog.setMessage(Preconditions.requireStringNotEmpty(message));
        return progressDialog;
    }

    private Toast createToast(View view, String message) {
        Toast toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_SHORT);
        ((TextView) view.findViewById(R.id.tv_message)).setText(message);
        toast.setView(view);
        return toast;
    }

    public Toast getAnnouncementToast(String message) {
        return createToast(getLayoutInflater().inflate(R.layout.layout_announcement_toast, new FrameLayout(context)), message);
    }

    public Toast getWarningToast(String message) {
        return createToast(getLayoutInflater().inflate(R.layout.layout_warning_toast, new FrameLayout(context)), message);
    }

    public Toast getSuccessToast(String message) {
        return createToast(getLayoutInflater().inflate(R.layout.layout_success_toast, new FrameLayout(context)), message);
    }

    public Toast getErrorToast(String message) {
        return createToast(getLayoutInflater().inflate(R.layout.layout_error_toast, new FrameLayout(context)), message);
    }

    public ProgressDialog getProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(context);
        }
        return progressDialog;
    }

    public void showKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    public void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}