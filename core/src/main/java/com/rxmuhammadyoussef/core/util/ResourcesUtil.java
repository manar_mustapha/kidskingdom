package com.rxmuhammadyoussef.core.util;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.inject.Inject;

import timber.log.Timber;

public class ResourcesUtil {

    private final Context context;

    @Inject
    public ResourcesUtil(Context context) {
        this.context = context;
    }

    public LayoutInflater getLayoutInflater() {
        return LayoutInflater.from(context);
    }

    public String getString(@StringRes int resourceId) {
        return context.getString(resourceId);
    }

    public int getColor(@ColorRes int colorRes) {
        return ContextCompat.getColor(context, colorRes);
    }

    public Bitmap getBitmapFromUrl(String imageUrl) throws IOException {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inSampleSize = calculateInSampleSize(options, 24, 24);
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        options.inJustDecodeBounds = false;
        URL url = new URL(imageUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setConnectTimeout(10000);
        connection.setReadTimeout(10000);
        Bitmap bmp = null;
        InputStream is = null;
        BufferedInputStream bis = null;
        try {
            is = connection.getInputStream();
            bis = new BufferedInputStream(is);
            bmp = BitmapFactory.decodeStream(bis, null, options);
        } catch (OutOfMemoryError e) {
            Timber.e(e);
        } finally {
            connection.disconnect();
            if (is != null)
                is.close();
            if (bis != null)
                bis.close();
        }
        return bmp;
    }

    private int calculateInSampleSize(@NonNull BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width lower or equal to the requested height and width.
            while ((height / inSampleSize) > reqHeight || (width / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    public Bitmap drawTextToBitmap(Bitmap bitmap,
                                   String gText) {
        Resources resources = context.getResources();
//        float scale = resources.getDisplayMetrics().density;
        android.graphics.Bitmap.Config bitmapConfig =
                bitmap.getConfig();
        if (bitmapConfig == null) {
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
        }
        bitmap = bitmap.copy(bitmapConfig, true);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        Paint paintRect = new Paint();
        paint.setColor(Color.rgb(225, 225, 225));
        paint.setTextSize(25);
        paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.setTypeface(Typeface.create("Arial", Typeface.NORMAL));
        String[] separated = gText.split("\n");
        Paint.FontMetrics fm = paint.getFontMetrics();
        float height = fm.descent - fm.ascent;
        Rect bounds = new Rect(0, 0, (int) paintRect.measureText(separated[1] + 30) * 2, ((int) height * separated.length) + 2);
        paintRect.setColor(Color.rgb(236, 26, 97));
        canvas.drawRect(bounds, paintRect);
        int lastHeight = 20;
        for (String aSeparated : separated) {
            String currentText = aSeparated.trim();
            paintRect.getTextBounds(currentText, 0, currentText.length(), bounds);
            canvas.drawText(currentText, 10, lastHeight + bounds.height(), paint);
            lastHeight += bounds.height() + 20;
        }
        return bitmap;
    }

    public Bitmap loadBitmapFromView(View view) {
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    public boolean isOrientationPortrait() {
        return Configuration.ORIENTATION_PORTRAIT == context.getResources().getConfiguration().orientation;
    }

    public Context getContext() {
        return context;
    }
}