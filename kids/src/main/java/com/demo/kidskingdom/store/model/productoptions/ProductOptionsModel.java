package com.demo.kidskingdom.store.model.productoptions;

public class ProductOptionsModel {

    private String attributeCode;
    private String value;

    ProductOptionsModel(String attributeCode, String value) {
        this.attributeCode = attributeCode;
        this.value = value;
    }

    public String getAttributeCode() {
        return attributeCode;
    }

    public String getValue() {
        return value;
    }
}