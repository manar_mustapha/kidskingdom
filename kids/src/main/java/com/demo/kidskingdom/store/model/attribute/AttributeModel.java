package com.demo.kidskingdom.store.model.attribute;

public class AttributeModel {

    private int sortOrder;
    private int attributeId;
    private int entityTypeId;
    private String attributeName;

    AttributeModel(int sortOrder, int attributeId, int entityTypeId, String attributeName) {
        this.sortOrder = sortOrder;
        this.attributeId = attributeId;
        this.entityTypeId = entityTypeId;
        this.attributeName = attributeName;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public int getAttributeId() {
        return attributeId;
    }

    public int getEntityTypeId() {
        return entityTypeId;
    }

    public String getAttributeName() {
        return attributeName;
    }
}