package com.demo.kidskingdom.store.model.attribute;

import com.google.gson.annotations.SerializedName;

public class AttributeApiResponse {

    @SerializedName("attribute_set_id")
    private int attributeId;
    @SerializedName("attribute_set_name")
    private String attributeName;
    @SerializedName("sort_order")
    private int sortOrder;
    @SerializedName("entity_type_id")
    private int entityTypeId;

    public int getAttributeId() {
        return attributeId;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public int getEntityTypeId() {
        return entityTypeId;
    }
}