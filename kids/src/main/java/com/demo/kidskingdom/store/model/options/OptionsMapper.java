package com.demo.kidskingdom.store.model.options;

import com.rxmuhammadyoussef.core.di.scope.ApplicationScope;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

@ApplicationScope
public class OptionsMapper {

    @Inject
    OptionsMapper() {

    }

    public OptionsModel toOptionsModel(OptionsApiResponse response) {
        return new OptionsModel(response.getLabel(),
                response.getValue());
    }

    public List<OptionsModel> toOptionsModels(List<OptionsApiResponse> responses) {
        List<OptionsModel> models = new ArrayList<>();
        for (OptionsApiResponse response : responses) {
            models.add(toOptionsModel(response));
        }
        return models;
    }

}