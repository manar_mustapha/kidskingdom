package com.demo.kidskingdom.store;

import com.demo.kidskingdom.store.api.ApiUtils;
import com.demo.kidskingdom.store.model.attribute.AttributeApiResponse;
import com.demo.kidskingdom.store.model.attribute.AttributeDTO;
import com.demo.kidskingdom.store.model.attributeparams.AttributeParamsApiResponse;
import com.demo.kidskingdom.store.model.product.ProductApiResponse;
import com.demo.kidskingdom.store.model.store.StoreApiResponse;
import com.rxmuhammadyoussef.core.di.scope.ApplicationScope;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

@ApplicationScope
public class WebServiceStore {

    private final ApiUtils apiUtils;
    private final String TOKEN = "onm881ubmp3qx757q3serh3ffe2nzjem";

    @Inject
    WebServiceStore(ApiUtils apiUtils) {
        this.apiUtils = apiUtils;
    }

    public Single<List<StoreApiResponse>> getAllStores() {
        return apiUtils.getKidsKingdomApiService()
                .getAllStores("Bearer ".concat(TOKEN));
    }

    public Single<List<AttributeApiResponse>> fetchAttributes() {
        return apiUtils.getKidsKingdomApiService()
                .fetchAttributes("Bearer ".concat(TOKEN), "attribute_set_name", "%", "like")
                .map(AttributeDTO::getAttributeApiResponses);
    }

    public Single<List<AttributeParamsApiResponse>> fetchAttributeParams(int entityTypeId) {
        return apiUtils.getKidsKingdomApiService()
                .fetchAttributeParams("Bearer ".concat(TOKEN), entityTypeId,
                        "entity_type_id", entityTypeId, "eq");
    }

    public Single<List<ProductApiResponse>> fetchProducts(String brand, String age, String size, String gender, String season, String code) {
        return apiUtils.getKidsKingdomWappService()
                .fetchProducts("Bearer ".concat(TOKEN), brand , age ,size , gender , season , code);
    }
}