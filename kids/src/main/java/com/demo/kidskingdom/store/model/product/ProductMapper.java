package com.demo.kidskingdom.store.model.product;

import com.rxmuhammadyoussef.core.di.scope.ApplicationScope;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

@ApplicationScope
public class ProductMapper {

    @Inject
    ProductMapper() {

    }

    public ProductModel toProductModel(ProductApiResponse response) {
        return new ProductModel(response.getImage(),
                response.getPrice(),
                response.getOriginalPrice(),
                response.getId(),
                response.getSku());
    }

    public List<ProductModel> toProductModels(List<ProductApiResponse> responses) {
        List<ProductModel> models = new ArrayList<>();
        for (ProductApiResponse response : responses) {
            models.add(toProductModel(response));
        }
        return models;
    }
}