package com.demo.kidskingdom.store.model.attribute;

import com.rxmuhammadyoussef.core.di.scope.ApplicationScope;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

@ApplicationScope
public class AttributeMapper {

    @Inject
    AttributeMapper() {

    }

    public AttributeModel toAttributeModel(AttributeApiResponse response) {
        return new AttributeModel(response.getSortOrder(),
                response.getAttributeId(),
                response.getEntityTypeId(),
                response.getAttributeName());
    }

    public List<AttributeModel> toAttributeModels(List<AttributeApiResponse> responses) {
        List<AttributeModel> models = new ArrayList<>();
        for (AttributeApiResponse response : responses) {
            models.add(toAttributeModel(response));
        }
        return models;
    }

}