package com.demo.kidskingdom.store.model.productoptions;

import com.google.gson.annotations.SerializedName;

public class ProductOptionsApiResponse {

    @SerializedName("attribute_code")
    private String attributeCode;
    @SerializedName("value")
    private String value;

    public String getAttributeCode() {
        return attributeCode;
    }

    public String getValue() {
        return value;
    }
}