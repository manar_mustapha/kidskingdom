package com.demo.kidskingdom.store.model.options;

public class OptionsModel {

    private String label;
    private String value;

    OptionsModel(String label, String value) {
        this.label = label;
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public String getValue() {
        return value;
    }
}