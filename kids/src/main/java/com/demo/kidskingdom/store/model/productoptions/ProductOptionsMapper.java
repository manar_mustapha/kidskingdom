package com.demo.kidskingdom.store.model.productoptions;

import com.rxmuhammadyoussef.core.di.scope.ApplicationScope;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

@ApplicationScope
public class ProductOptionsMapper {

    @Inject
    ProductOptionsMapper() {

    }

    public ProductOptionsModel toModel(ProductOptionsApiResponse response) {
        return new ProductOptionsModel(response.getAttributeCode(),
                response.getValue());
    }

    public List<ProductOptionsModel> toModels(List<ProductOptionsApiResponse> responses) {
        List<ProductOptionsModel> models = new ArrayList<>();
        for (ProductOptionsApiResponse response : responses) {
            models.add(toModel(response));
        }
        return models;
    }
}