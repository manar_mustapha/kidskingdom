package com.demo.kidskingdom.store.model.product;

public class ProductModel {

    private String image;
    private String price;
    private String originalPrice;
    private String id;
    private String sku;

    ProductModel(String image, String price, String originalPrice, String id, String sku) {
        this.image = image;
        this.price = price;
        this.originalPrice = originalPrice;
        this.id = id;
        this.sku = sku;
    }

    public String getImage() {
        return image;
    }

    public String getPrice() {
        return price;
    }

    public String getOriginalPrice() {
        return originalPrice;
    }

    public String getId() {
        return id;
    }

    public String getSku() {
        return sku;
    }
}