package com.demo.kidskingdom.store.api;

import com.demo.kidskingdom.store.model.product.ProductApiResponse;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface KidsKingdomWappService {

    @GET("whatsapp_api")
    Single<List<ProductApiResponse>> fetchProducts(
            @Header("Authorization") String header,
            @Query("brands") String brand,
            @Query("age") String age,
            @Query("size") String size,
            @Query("gender") String gender,
            @Query("season") String season,
            @Query("sku") String code);
}
