package com.demo.kidskingdom.store.model.store;

import com.google.gson.annotations.SerializedName;

public class StoreApiResponse {
    
    @SerializedName("id")
    private int id;
    @SerializedName("code")
    private String code;
    @SerializedName("name")
    private String name;
    @SerializedName("default_group_id")
    private int defaultGroupId;

    public int getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public int getDefaultGroupId() {
        return defaultGroupId;
    }
}
