package com.demo.kidskingdom.store.model.attributeparams;

import com.demo.kidskingdom.store.model.options.OptionsModel;

import java.util.List;

public class AttributeParamsModel {

    private int attributeId;
    private String attributeCode;
    private int entityTypeId;
    private List<OptionsModel> options;

    AttributeParamsModel(int attributeId, String attributeCode, int entityTypeId, List<OptionsModel> options) {
        this.options = options;
        this.attributeId = attributeId;
        this.attributeCode = attributeCode;
        this.entityTypeId = entityTypeId;
    }

    public int getAttributeId() {
        return attributeId;
    }

    public String getAttributeCode() {
        return attributeCode;
    }

    public int getEntityTypeId() {
        return entityTypeId;
    }

    public List<OptionsModel> getOptions() {
        return options;
    }
}