package com.demo.kidskingdom.store.model.attributeparams;

import com.demo.kidskingdom.store.model.options.OptionsMapper;
import com.rxmuhammadyoussef.core.di.scope.ApplicationScope;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

@ApplicationScope
public class AttributeParamsMapper {

    private final OptionsMapper optionsMapper;

    @Inject
    AttributeParamsMapper(OptionsMapper optionsMapper) {
        this.optionsMapper = optionsMapper;
    }

    public AttributeParamsModel toAttributeParamsModel(AttributeParamsApiResponse response) {
        return new AttributeParamsModel(response.getAttributeId(),
                response.getAttributeCode(),
                response.getEntityTypeId(),
                optionsMapper.toOptionsModels(response.getOptions()));
    }

    public List<AttributeParamsModel> toAttributeParamsModels(List<AttributeParamsApiResponse> responses) {
        List<AttributeParamsModel> models = new ArrayList<>();
        for (AttributeParamsApiResponse response : responses) {
            models.add(toAttributeParamsModel(response));
        }
        return models;
    }
}