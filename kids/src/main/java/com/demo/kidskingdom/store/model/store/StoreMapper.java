package com.demo.kidskingdom.store.model.store;

import com.rxmuhammadyoussef.core.di.scope.ApplicationScope;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

@ApplicationScope
public class StoreMapper {

    @Inject
    StoreMapper() {

    }

    public StoreModel toStoreModel(StoreApiResponse response) {
        return new StoreModel(response.getId(),
                response.getCode(),
                response.getName(),
                response.getDefaultGroupId());
    }

    public List<StoreModel> toStoreModels(List<StoreApiResponse> responses) {
        List<StoreModel> models = new ArrayList<>();
        for (StoreApiResponse response : responses) {
            models.add(toStoreModel(response));
        }
        return models;
    }
}