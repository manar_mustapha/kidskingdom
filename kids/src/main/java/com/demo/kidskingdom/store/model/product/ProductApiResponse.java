package com.demo.kidskingdom.store.model.product;

import com.google.gson.annotations.SerializedName;

public class ProductApiResponse {

    @SerializedName("image")
    private String image;
    @SerializedName("price")
    private String price;
    @SerializedName("originalPrice")
    private String originalPrice;
    @SerializedName("id")
    private String id;
    @SerializedName("sku")
    private String sku;

    public String getImage() {
        return image;
    }

    public String getPrice() {
        return price;
    }

    public String getOriginalPrice() {
        return originalPrice;
    }

    public String getId() {
        return id;
    }

    public String getSku() {
        return sku;
    }

}