package com.demo.kidskingdom.store.model.attributeparams;

import com.demo.kidskingdom.store.model.options.OptionsApiResponse;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AttributeParamsApiResponse {

    @SerializedName("attribute_id")
    private int attributeId;
    @SerializedName("attribute_code")
    private String attributeCode;
    @SerializedName("entity_type_id")
    private int entityTypeId;
    @SerializedName("options")
    private List<OptionsApiResponse> options;

    public int getAttributeId() {
        return attributeId;
    }

    public String getAttributeCode() {
        return attributeCode;
    }

    public int getEntityTypeId() {
        return entityTypeId;
    }

    public List<OptionsApiResponse> getOptions() {
        return options;
    }
}