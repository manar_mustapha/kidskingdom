package com.demo.kidskingdom.store.model.attribute;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AttributeDTO {

    @SerializedName("items")
    private List<AttributeApiResponse> attributeApiResponses;

    public List<AttributeApiResponse> getAttributeApiResponses() {
        return attributeApiResponses;
    }
}