package com.demo.kidskingdom.store.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.rxmuhammadyoussef.core.schedulers.NetworkThreadSchedulers;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiUtils {

    public static final String BASE_URL = "http://www.kidskingdom.world/";
    public static final String WAPP_BASE_URL = "http://kidskingdom.world/";
    public static final String USER_URL = "ar/rest/all/V1/";
    public static final String IMAGE_URL = "http://www.kidskingdom.world/pub/media/catalog/product";
    private static Retrofit userRetrofit;
    private static Retrofit wappRetrofit;
    private final OkHttpClient okHttpClient = createOkHttpClient();
    private Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    @Inject
    ApiUtils() {

    }

    public KidsKingdomApiService getKidsKingdomApiService() {
        return getUserRetrofit().create(KidsKingdomApiService.class);
    }

    public KidsKingdomWappService getKidsKingdomWappService() {
        return getWappRetrofit().create(KidsKingdomWappService.class);
    }

    private Retrofit getUserRetrofit() {
        if (ApiUtils.userRetrofit == null) {
            ApiUtils.userRetrofit = new Retrofit.Builder()
                    .baseUrl(ApiUtils.BASE_URL.concat(ApiUtils.USER_URL))
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(new NetworkThreadSchedulers().workerThread()))
                    .client(okHttpClient)
                    .build();
        }
        return ApiUtils.userRetrofit;
    }

    private Retrofit getWappRetrofit() {
        if (ApiUtils.wappRetrofit== null) {
            ApiUtils.wappRetrofit = new Retrofit.Builder()
                    .baseUrl(ApiUtils.WAPP_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(new NetworkThreadSchedulers().workerThread()))
                    .client(okHttpClient)
                    .build();
        }
        return ApiUtils.wappRetrofit;
    }

    private OkHttpClient createOkHttpClient() {
        return new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(20, TimeUnit.SECONDS)
                .build();
    }
}