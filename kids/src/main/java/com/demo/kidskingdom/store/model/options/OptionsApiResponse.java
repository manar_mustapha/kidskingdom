package com.demo.kidskingdom.store.model.options;

import com.google.gson.annotations.SerializedName;

public class OptionsApiResponse {

    @SerializedName("label")
    private String label;
    @SerializedName("value")
    private String value;

    public String getLabel() {
        return label;
    }

    public String getValue() {
        return value;
    }
}