package com.demo.kidskingdom.store.model.store;

public class StoreModel {

    private int id;
    private String code;
    private String name;
    private int defaultGroupId;

    StoreModel(int id, String code, String name, int defaultGroupId) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.defaultGroupId = defaultGroupId;
    }

    public int getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public int getDefaultGroupId() {
        return defaultGroupId;
    }
}