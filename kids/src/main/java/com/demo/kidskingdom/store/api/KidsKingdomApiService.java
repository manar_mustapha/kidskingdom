package com.demo.kidskingdom.store.api;

import com.demo.kidskingdom.store.model.attribute.AttributeDTO;
import com.demo.kidskingdom.store.model.attributeparams.AttributeParamsApiResponse;
import com.demo.kidskingdom.store.model.product.ProductApiResponse;
import com.demo.kidskingdom.store.model.store.StoreApiResponse;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface KidsKingdomApiService {

    @GET("store/websites")
    Single<List<StoreApiResponse>> getAllStores(
            @Header("Authorization") String header);

    @GET("products/attribute-sets/sets/list")
    Single<AttributeDTO> fetchAttributes(
            @Header("Authorization") String header,
            @Query("searchCriteria[filterGroups][0][filters][0][field]") String field,
            @Query("searchCriteria[filterGroups][0][filters][0][value]") String value,
            @Query("searchCriteria[filterGroups][0][filters][0][conditionType]") String conditionType);

    @GET("products/attribute-sets/{attribute_set_id}/attributes")
    Single<List<AttributeParamsApiResponse>> fetchAttributeParams(
            @Header("Authorization") String header,
            @Path("attribute_set_id") int entityTypeId,
            @Query("searchCriteria[filterGroups][0][filters][0][field]") String entity_type_id,
            @Query("searchCriteria[filterGroups][0][filters][0][value]") int entityIdValue,
            @Query("searchCriteria[filterGroups][0][filters][0][conditionType]") String condition);

}