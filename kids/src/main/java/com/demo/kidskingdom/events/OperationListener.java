package com.demo.kidskingdom.events;

public interface OperationListener<T> {

    void onPreOperation();

    void onPostOperation();

    void onSuccess(T element);

    void onError(Throwable t);
}