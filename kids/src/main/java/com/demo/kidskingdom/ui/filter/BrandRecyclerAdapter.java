package com.demo.kidskingdom.ui.filter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.demo.kidskingdom.R;
import com.demo.kidskingdom.store.model.options.OptionsModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BrandRecyclerAdapter extends RecyclerView.Adapter<BrandRecyclerAdapter.BrandViewHolder> {

    private BrandRecyclerAdapter.OnItemClickListener onItemClickListener;
    private List<OptionsModel> selectedStoreModels;
    private LayoutInflater layoutInflater;
    private List<OptionsModel> sizeModels;

    BrandRecyclerAdapter(Context context, BrandRecyclerAdapter.OnItemClickListener onItemClickListener) {
        this.layoutInflater = LayoutInflater.from(context);
        this.onItemClickListener = onItemClickListener;
        this.selectedStoreModels = new ArrayList<>();
        this.sizeModels = new ArrayList<>();
    }

    void setData(List<OptionsModel> newStoreModels) {
        this.sizeModels.clear();
        this.sizeModels.addAll(newStoreModels);
        notifyDataSetChanged();
    }

    @Override
    public BrandRecyclerAdapter.BrandViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BrandRecyclerAdapter.BrandViewHolder(layoutInflater.inflate(R.layout.item_store, parent, false));
    }

    String getSelectedIds() {
        List<String> selectedStoresName = new ArrayList<>();
        for (OptionsModel model : selectedStoreModels) {
            selectedStoresName.add(model.getValue());
        }
        return selectedStoresName.toString()
                .replace("[", "")
                .replace("]", "");
    }

    @Override
    public void onBindViewHolder(BrandRecyclerAdapter.BrandViewHolder holder, int position) {
        holder.bind(sizeModels.get(position));
    }

    @Override
    public int getItemCount() {
        return sizeModels.size();
    }

    public interface OnItemClickListener {
        void onItemClicked(String viewModel);
    }

    class BrandViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title)
        TextView titleTextView;
        private OptionsModel storeModel;

        BrandViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> {
                if (titleTextView.isActivated()) {
                    selectedStoreModels.remove(storeModel);
                } else {
                    selectedStoreModels.add(storeModel);
                }
                List<String> selectedStoresName = new ArrayList<>();
                for (OptionsModel model : selectedStoreModels) {
                    selectedStoresName.add(model.getLabel());
                }
                onItemClickListener.onItemClicked(selectedStoresName.toString()
                        .replace("[", "")
                        .replace("]", ""));
                titleTextView.setActivated(!titleTextView.isActivated());
            });
        }

        void bind(OptionsModel storeModel) {
            this.storeModel = storeModel;
            titleTextView.setText(storeModel.getLabel());
            titleTextView.setActivated(selectedStoreModels.contains(storeModel));
        }
    }
}