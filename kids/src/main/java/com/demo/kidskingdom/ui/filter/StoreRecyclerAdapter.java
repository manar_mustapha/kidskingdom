package com.demo.kidskingdom.ui.filter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.demo.kidskingdom.R;
import com.demo.kidskingdom.store.model.store.StoreModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoreRecyclerAdapter extends RecyclerView.Adapter<StoreRecyclerAdapter.StoreViewHolder> {

    private OnItemClickListener onItemClickListener;
    private List<StoreModel> selectedStoreModels;
    private LayoutInflater layoutInflater;
    private List<StoreModel> storeModels;

    StoreRecyclerAdapter(Context context, OnItemClickListener onItemClickListener) {
        this.layoutInflater = LayoutInflater.from(context);
        this.onItemClickListener = onItemClickListener;
        this.selectedStoreModels = new ArrayList<>();
        this.storeModels = new ArrayList<>();
    }

    void setData(List<StoreModel> newStoreModels) {
        this.storeModels.clear();
        this.storeModels.addAll(newStoreModels);
        notifyDataSetChanged();
    }

    String getSelectedIds() {
        List<String> selectedStoresName = new ArrayList<>();
        for (StoreModel model : selectedStoreModels) {
            selectedStoresName.add(String.valueOf(model.getId()));
        }
        return selectedStoresName.toString()
                .replace("[", "")
                .replace("]", "");
    }

    @Override
    public StoreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new StoreViewHolder(layoutInflater.inflate(R.layout.item_store, parent, false));
    }

    @Override
    public void onBindViewHolder(StoreViewHolder holder, int position) {
        holder.bind(storeModels.get(position));
    }

    @Override
    public int getItemCount() {
        return storeModels.size();
    }

    public interface OnItemClickListener {
        void onItemClicked(String viewModel);
    }

    class StoreViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title)
        TextView titleTextView;
        private StoreModel storeModel;

        StoreViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> {
                if (titleTextView.isActivated()) {
                    selectedStoreModels.remove(storeModel);
                } else {
                    selectedStoreModels.add(storeModel);
                }
                List<String> selectedStoresName = new ArrayList<>();
                for (StoreModel model :
                        selectedStoreModels) {
                    selectedStoresName.add(model.getName());
                }
                onItemClickListener.onItemClicked(selectedStoresName.toString()
                        .replace("[", "")
                        .replace("]", ""));
                titleTextView.setActivated(!titleTextView.isActivated());
            });
        }

        void bind(StoreModel storeModel) {
            this.storeModel = storeModel;
            titleTextView.setText(storeModel.getName());
            titleTextView.setActivated(selectedStoreModels.contains(storeModel));
        }
    }
}