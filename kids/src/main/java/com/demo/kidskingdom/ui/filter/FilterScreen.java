package com.demo.kidskingdom.ui.filter;

import com.demo.kidskingdom.store.model.options.OptionsModel;
import com.demo.kidskingdom.store.model.store.StoreModel;
import com.rxmuhammadyoussef.core.component.activity.BaseActivityScreen;

import java.util.List;

public interface FilterScreen extends BaseActivityScreen {

    void onStoreClicked(List<StoreModel> storeModels);

    void onSizeClicked(List<OptionsModel> size);

    void setupAdapters();

    void onSeasonClicked(List<OptionsModel> season);

    void onAgeClicked(List<OptionsModel> age);

    void onGenderClicked(List<OptionsModel> gender);

    void onBrandClicked(List<OptionsModel> brands);

    void showWappPopup();

    void shouldNavigateToDirectory(String directory);
}