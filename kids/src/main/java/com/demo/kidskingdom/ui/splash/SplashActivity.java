package com.demo.kidskingdom.ui.splash;

import android.content.Intent;
import android.os.Handler;

import com.demo.kidskingdom.KidsApplication;
import com.demo.kidskingdom.R;
import com.demo.kidskingdom.di.activity.ActivityModule;
import com.demo.kidskingdom.ui.filter.FilterActivity;
import com.rxmuhammadyoussef.core.component.activity.BaseActivity;
import com.rxmuhammadyoussef.core.component.activity.BaseActivityScreen;
import com.rxmuhammadyoussef.core.di.scope.ActivityScope;

import butterknife.ButterKnife;

@ActivityScope
public class SplashActivity extends BaseActivity implements BaseActivityScreen {

    @Override
    protected void onCreateActivityComponents() {
        KidsApplication.getComponent(this)
                .plus(new ActivityModule(this))
                .inject(this);
        ButterKnife.bind(this);
        new Handler().postDelayed(() -> {
            startActivity(new Intent(this, FilterActivity.class));
            finish();
        }, 1000);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_splash;
    }
}