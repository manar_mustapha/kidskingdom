package com.demo.kidskingdom.ui.filter;

import android.graphics.Bitmap;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;

import com.demo.kidskingdom.R;
import com.demo.kidskingdom.store.WebServiceStore;
import com.demo.kidskingdom.store.model.attribute.AttributeMapper;
import com.demo.kidskingdom.store.model.attribute.AttributeModel;
import com.demo.kidskingdom.store.model.attributeparams.AttributeParamsMapper;
import com.demo.kidskingdom.store.model.attributeparams.AttributeParamsModel;
import com.demo.kidskingdom.store.model.options.OptionsModel;
import com.demo.kidskingdom.store.model.product.ProductMapper;
import com.demo.kidskingdom.store.model.product.ProductModel;
import com.demo.kidskingdom.store.model.store.StoreMapper;
import com.demo.kidskingdom.store.model.store.StoreModel;
import com.jakewharton.rxrelay2.BehaviorRelay;
import com.rxmuhammadyoussef.core.component.activity.BaseActivityPresenter;
import com.rxmuhammadyoussef.core.di.qualifier.ForActivity;
import com.rxmuhammadyoussef.core.di.scope.ActivityScope;
import com.rxmuhammadyoussef.core.schedulers.ThreadSchedulers;
import com.rxmuhammadyoussef.core.schedulers.qualifires.IOThread;
import com.rxmuhammadyoussef.core.util.FileUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

@ActivityScope
class FilterPresenter extends BaseActivityPresenter {

    private final BehaviorRelay<List<AttributeParamsModel>> attributeParamsModelsRelay;
    private final BehaviorRelay<List<AttributeModel>> attributeModelsRelay;
    private final HashMap<String, List<OptionsModel>> filterHashMap;
    private final BehaviorRelay<List<StoreModel>> storeModelsRelay;
    private final AttributeParamsMapper attributeParamsMapper;
    private final ThreadSchedulers threadSchedulers;
    private final WebServiceStore webServiceStore;
    private final AttributeMapper attributeMapper;
    private final CompositeDisposable disposable;
    private final ProductMapper productMapper;
    private final FilterScreen filterScreen;
    private final StoreMapper storeMapper;
    private boolean didFindData = false;
    private int counter = 0;
    private String directory;

    @Inject
    FilterPresenter(AttributeParamsMapper attributeParamsMapper,
                    @IOThread ThreadSchedulers threadSchedulers,
                    @ForActivity CompositeDisposable disposable,
                    WebServiceStore webServiceStore,
                    AttributeMapper attributeMapper,
                    ProductMapper productMapper,
                    FilterScreen filterScreen,
                    StoreMapper storeMapper) {
        super(filterScreen);
        this.attributeParamsModelsRelay = BehaviorRelay.createDefault(Collections.emptyList());
        this.attributeModelsRelay = BehaviorRelay.createDefault(Collections.emptyList());
        this.storeModelsRelay = BehaviorRelay.createDefault(Collections.emptyList());
        this.attributeParamsMapper = attributeParamsMapper;
        this.threadSchedulers = threadSchedulers;
        this.webServiceStore = webServiceStore;
        this.attributeMapper = attributeMapper;
        this.filterHashMap = new HashMap<>();
        this.productMapper = productMapper;
        this.filterScreen = filterScreen;
        this.storeMapper = storeMapper;
        this.disposable = disposable;
    }

    @Override
    protected void onCreate() {
        filterScreen.setupAdapters();
        fetchAttributes();
        fetchStores();
    }

    private void fetchAttributes() {
        disposable.add(webServiceStore.fetchAttributes()
                .map(attributeMapper::toAttributeModels)
                .subscribeOn(threadSchedulers.workerThread())
                .observeOn(threadSchedulers.mainThread())
                .doOnSubscribe(ignored -> filterScreen.showLoadingAnimation())
                .doFinally(filterScreen::hideLoadingAnimation)
                .subscribe(attributeModels -> {
                    attributeModelsRelay.accept(attributeModels);
                    executeAttributeParams(attributeModelsRelay.getValue().get(counter).getAttributeId());
                }, Timber::e));
    }

    private void fetchStores() {
        disposable.add(webServiceStore.getAllStores()
                .map(storeMapper::toStoreModels)
                .subscribeOn(threadSchedulers.workerThread())
                .observeOn(threadSchedulers.mainThread())
                .subscribe(storeModelsRelay, Timber::e));
    }

    private void executeAttributeParams(int attributeId) {
        disposable.add(webServiceStore.fetchAttributeParams(attributeId)
                .map(attributeParamsMapper::toAttributeParamsModels)
                .subscribeOn(threadSchedulers.workerThread())
                .observeOn(threadSchedulers.mainThread())
                .subscribe(attributeParamsModels -> {
                    List<AttributeParamsModel> temp = new ArrayList<>(attributeParamsModelsRelay.getValue());
                    temp.addAll(attributeParamsModels);
                    attributeParamsModelsRelay.accept(temp);
                    convertArrayToHashMap();
                    counter++;
                    if (counter < attributeModelsRelay.getValue().size()) {
                        executeAttributeParams(attributeModelsRelay.getValue().get(counter).getAttributeId());
                    }
                }, Timber::e));
    }

    private void convertArrayToHashMap() {
        for (AttributeParamsModel model : attributeParamsModelsRelay.getValue()) {
            if (filterHashMap.containsKey(model.getAttributeCode())) {
                filterHashMap.get(model.getAttributeCode()).clear();
                filterHashMap.get(model.getAttributeCode()).addAll(model.getOptions());
            } else {
                filterHashMap.put(model.getAttributeCode(), model.getOptions());
            }
        }
    }

    void onStoreClicked() {
        filterScreen.onStoreClicked(storeModelsRelay.getValue());
    }

    void onSizeClicked() {
        if (filterHashMap.get("size") != null) {
            filterScreen.onSizeClicked(filterHashMap.get("size"));
        }
    }

    void onSeasonClicked() {
        if (filterHashMap.get("season") != null) {
            filterScreen.onSeasonClicked(filterHashMap.get("season"));
        }
    }

    void onAgeClicked() {
        if (filterHashMap.get("age") != null) {
            filterScreen.onAgeClicked(filterHashMap.get("age"));
        }
    }

    void onGenderClicked() {
        if (filterHashMap.get("gender") != null) {
            filterScreen.onGenderClicked(filterHashMap.get("gender"));
        }
    }

    void onBrandClicked() {
        if (filterHashMap.get("brands") != null) {
            filterScreen.onBrandClicked(filterHashMap.get("brands"));
        }
    }

    void onFilterClicked(String brand, String age, String size, String gender, String season, String code) {
        disposable.add(webServiceStore.fetchProducts(brand, age, size, gender, season, code)
                .map(productMapper::toProductModels)
                .subscribeOn(threadSchedulers.workerThread())
                .observeOn(threadSchedulers.mainThread())
                .doOnSubscribe(ignored -> filterScreen.showLoadingAnimation())
                .subscribe(this::processResult, throwable -> {
                    if (throwable instanceof com.jakewharton.retrofit2.adapter.rxjava2.HttpException) {
                        filterScreen.showErrorMessage(throwable.getMessage());
                        filterScreen.hideLoadingAnimation();
                    }
                }));
    }

    private void processResult(List<ProductModel> productModels) {
        if (getPermissionUtil().hasReadWritePermission()) {
            Handler handler = new Handler();
            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        if (Looper.myLooper() == null) {
                            Looper.prepare();
                        }
                        if (Looper.myLooper() == Looper.getMainLooper()) {
                            Timber.d("Warning your Thread on UI Thread.....#####");
                        }
                        directory = "KidsKingdom/".concat(String.valueOf(System.currentTimeMillis()));
                        for (ProductModel product : productModels) {
                            if (FileUtil.isImageExtension(FileUtil.getExtension(product.getImage()))) {
                                Bitmap newBitmap = getResourcesUtil().drawTextToBitmap(getResourcesUtil().getBitmapFromUrl(product.getImage()),
                                        "Code \n".concat(product.getSku())
                                                .concat("\n")
                                                .concat("Price \n")
                                                .concat(product.getOriginalPrice())
                                                .concat("\n"));
                                new ImageSaver(getResourcesUtil().getContext())
                                        .setFileName(String.valueOf(System.currentTimeMillis()).concat(".").concat("png"))
                                        .setDirectoryName(directory)
                                        .setExternal(true)
                                        .save(newBitmap);
                                didFindData = true;
                            }
                        }
                        filterScreen.hideLoadingAnimation();
                        if (didFindData) {
                            didFindData = false;
                            handler.post(filterScreen::showWappPopup);
                        } else {
                            handler.post(() -> filterScreen.showWarningMessage(getResourcesUtil().getString(R.string.no_matched_data)));
                        }
                    } catch (Exception e) {
                        Timber.e(e);
                        filterScreen.showErrorMessage(e.getMessage());
                        filterScreen.hideLoadingAnimation();
                    }
                }
            };
            thread.start();
        } else {
            getPermissionUtil().requestReadWritePermission();
            filterScreen.hideLoadingAnimation();
        }
    }

    void onDeleteClicked() {
        File fileOrDirectory = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "KidsKingdom");
        deleteRecursive(fileOrDirectory);
    }

    private void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles()) {
                deleteRecursive(child);
            }
        }
        fileOrDirectory.delete();
    }

    void openDirectory() {
        filterScreen.shouldNavigateToDirectory(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES).getAbsolutePath().concat("/").concat(directory));
    }
}