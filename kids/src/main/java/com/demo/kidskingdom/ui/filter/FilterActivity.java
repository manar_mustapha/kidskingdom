package com.demo.kidskingdom.ui.filter;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.Gravity;

import com.afollestad.materialdialogs.MaterialDialog;
import com.demo.kidskingdom.KidsApplication;
import com.demo.kidskingdom.R;
import com.demo.kidskingdom.di.activity.ActivityModule;
import com.demo.kidskingdom.store.model.options.OptionsModel;
import com.demo.kidskingdom.store.model.store.StoreModel;
import com.rxmuhammadyoussef.core.component.activity.BaseActivity;
import com.rxmuhammadyoussef.core.di.scope.ActivityScope;
import com.rxmuhammadyoussef.core.widget.rxedittext.RxEditText;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@ActivityScope
public class FilterActivity extends BaseActivity implements FilterScreen {

    @Inject
    FilterPresenter presenter;
    @BindView(R.id.et_store)
    RxEditText storeEditText;
    @BindView(R.id.et_age)
    RxEditText ageEditText;
    @BindView(R.id.et_brand)
    RxEditText brandEditText;
    @BindView(R.id.et_weather)
    RxEditText seasonEditText;
    @BindView(R.id.et_size)
    RxEditText sizeEditText;
    @BindView(R.id.et_gender)
    RxEditText genderEditText;
    @BindView(R.id.et_code)
    RxEditText codeEditText;
    private MaterialDialog materialDialog;
    private SeasonRecyclerAdapter seasonRecyclerAdapter;
    private GenderRecyclerAdapter genderRecyclerAdapter;
    private BrandRecyclerAdapter brandRecyclerAdapter;
    private StoreRecyclerAdapter storeRecyclerAdapter;
    private SizeRecyclerAdapter sizeRecyclerAdapter;
    private AgeRecyclerAdapter ageRecyclerAdapter;
    private String number;

    @Override
    protected void onCreateActivityComponents() {
        KidsApplication.getComponent(this)
                .plus(new ActivityModule(this))
                .inject(this);
        ButterKnife.bind(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_filter;
    }

    @Override
    public void setupAdapters() {
        seasonRecyclerAdapter = new SeasonRecyclerAdapter(this, this::updateSeason);
        genderRecyclerAdapter = new GenderRecyclerAdapter(this, this::updateGender);
        storeRecyclerAdapter = new StoreRecyclerAdapter(this, this::updateStore);
        brandRecyclerAdapter = new BrandRecyclerAdapter(this, this::updateBrand);
        sizeRecyclerAdapter = new SizeRecyclerAdapter(this, this::updateSize);
        ageRecyclerAdapter = new AgeRecyclerAdapter(this, this::updateAge);
    }

    @OnClick(R.id.et_store)
    void onStoreClicked() {
        presenter.onStoreClicked();
    }

    @Override
    public void onStoreClicked(List<StoreModel> storeModels) {
        storeRecyclerAdapter.setData(storeModels);
        materialDialog = new MaterialDialog.Builder(this)
                .adapter(storeRecyclerAdapter, new LinearLayoutManager(this))
                .backgroundColor(getResources().getColor(R.color.transparent))
                .build();
        materialDialog.getRecyclerView().setBackground(getResources().getDrawable(R.drawable.shape_rounded_white));
        materialDialog.show();
    }

    private void updateStore(String selectedStoreModels) {
        storeEditText.setText(selectedStoreModels);
    }

    @OnClick(R.id.et_size)
    void onSizeClicked() {
        presenter.onSizeClicked();
    }

    @Override
    public void onSizeClicked(List<OptionsModel> sizeList) {
        sizeRecyclerAdapter.setData(sizeList);
        materialDialog = new MaterialDialog.Builder(this)
                .adapter(sizeRecyclerAdapter, new LinearLayoutManager(this))
                .backgroundColor(getResources().getColor(R.color.transparent))
                .build();
        materialDialog.getRecyclerView().setBackground(getResources().getDrawable(R.drawable.shape_rounded_white));
        materialDialog.show();
    }

    private void updateSize(String sizes) {
        sizeEditText.setText(sizes);
    }

    @OnClick(R.id.et_weather)
    void onWeatherClicked() {
        presenter.onSeasonClicked();
    }

    @Override
    public void onSeasonClicked(List<OptionsModel> season) {
        seasonRecyclerAdapter.setData(season);
        materialDialog = new MaterialDialog.Builder(this)
                .adapter(seasonRecyclerAdapter, new LinearLayoutManager(this))
                .backgroundColor(getResources().getColor(R.color.transparent))
                .build();
        materialDialog.getRecyclerView().setBackground(getResources().getDrawable(R.drawable.shape_rounded_white));
        materialDialog.show();
    }

    private void updateSeason(String seasons) {
        seasonEditText.setText(seasons);
    }

    @OnClick(R.id.et_age)
    void onAgeClicked() {
        presenter.onAgeClicked();
    }

    @Override
    public void onAgeClicked(List<OptionsModel> age) {
        ageRecyclerAdapter.setData(age);
        materialDialog = new MaterialDialog.Builder(this)
                .adapter(ageRecyclerAdapter, new LinearLayoutManager(this))
                .backgroundColor(getResources().getColor(R.color.transparent))
                .build();
        materialDialog.getRecyclerView().setBackground(getResources().getDrawable(R.drawable.shape_rounded_white));
        materialDialog.show();
    }

    private void updateAge(String age) {
        ageEditText.setText(age);
    }

    @OnClick(R.id.et_gender)
    void onGenderClicked() {
        presenter.onGenderClicked();
    }

    @Override
    public void onGenderClicked(List<OptionsModel> age) {
        genderRecyclerAdapter.setData(age);
        materialDialog = new MaterialDialog.Builder(this)
                .adapter(genderRecyclerAdapter, new LinearLayoutManager(this))
                .backgroundColor(getResources().getColor(R.color.transparent))
                .build();
        materialDialog.getRecyclerView().setBackground(getResources().getDrawable(R.drawable.shape_rounded_white));
        materialDialog.show();
    }

    private void updateGender(String gender) {
        genderEditText.setText(gender);
    }

    @OnClick(R.id.et_brand)
    void onBrandClicked() {
        presenter.onBrandClicked();
    }

    @Override
    public void onBrandClicked(List<OptionsModel> brand) {
        brandRecyclerAdapter.setData(brand);
        materialDialog = new MaterialDialog.Builder(this)
                .adapter(brandRecyclerAdapter, new LinearLayoutManager(this))
                .backgroundColor(getResources().getColor(R.color.transparent))
                .build();
        materialDialog.getRecyclerView().setBackground(getResources().getDrawable(R.drawable.shape_rounded_white));
        materialDialog.show();
    }

    private void updateBrand(String brands) {
        brandEditText.setText(brands);
    }

    @OnClick(R.id.tv_filter)
    void onFilterClicked() {
        presenter.onFilterClicked(brandRecyclerAdapter.getSelectedIds().replace(" ", ""),
                ageRecyclerAdapter.getSelectedIds().replace(" ", ""),
                sizeRecyclerAdapter.getSelectedIds().replace(" ", ""),
                genderRecyclerAdapter.getSelectedIds().replace(" ", ""),
                seasonRecyclerAdapter.getSelectedIds().replace(" ", ""),
                codeEditText.getText().toString());
    }

    @Override
    public void showWappPopup() {
        getUiUtil().getDialogBuilder(this, R.layout.wapp_layout)
                .editText(R.id.et_review, text -> this.number = text)
                .clickListener(R.id.tv_submit, (dialog, view) -> openWhatsApp(number))
                .clickListener(R.id.tv_directory, (dialog, view) -> presenter.openDirectory())
                .clickListener(R.id.iv_close, (dialog, view) -> dialog.dismiss())
                .background(R.drawable.inset_bottomsheet_background)
                .gravity(Gravity.CENTER)
                .cancelable(false)
                .build()
                .show();
    }

    @Override
    public void shouldNavigateToDirectory(String directory) {
        try {
            File path = new File(directory);
            Intent intent = getPackageManager().getLaunchIntentForPackage("com.sec.android.app.myfiles");
            intent.setAction("samsung.myfiles.intent.action.LAUNCH_MY_FILES");
            intent.putExtra("samsung.myfiles.intent.extra.START_PATH", path.getAbsolutePath());
            startActivity(intent);
        } catch (Exception e) {
            showErrorMessage(e.getMessage());
        }
    }

    private void openWhatsApp(String number) {
        try {
            String concatUrl = "";
            if (!TextUtils.isEmpty(ageEditText.getText().toString())) {
                concatUrl += "Age : ".concat(ageEditText.getText().toString()).concat("%0D%0A");
            }
            if (!TextUtils.isEmpty(genderEditText.getText().toString())) {
                concatUrl += "Gender : ".concat(genderEditText.getText().toString()).concat("%0D%0A");
            }
            if (!TextUtils.isEmpty(seasonEditText.getText().toString())) {
                concatUrl += "Season : ".concat(seasonEditText.getText().toString()).concat("%0D%0A");
            }
            if (!TextUtils.isEmpty(brandEditText.getText().toString())) {
                concatUrl += "Brand : ".concat(brandEditText.getText().toString()).concat("%0D%0A");
            }
            if (!TextUtils.isEmpty(sizeEditText.getText().toString())) {
                concatUrl += "Size : ".concat(sizeEditText.getText().toString()).concat("%0D%0A");
            }
            if (!TextUtils.isEmpty(codeEditText.getText().toString())) {
                concatUrl += "Code : ".concat(codeEditText.getText().toString()).concat("%0D%0A");
            }
            String url = "https://wa.me/".concat(number).concat("?text=").concat(concatUrl);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            startActivity(intent);
        } catch (Exception e) {
            showErrorMessage(e.getMessage());
            e.printStackTrace();
        }
    }

    @OnClick(R.id.tv_reset)
    void onResetClicked() {
        ageEditText.setText("");
        sizeEditText.setText("");
        codeEditText.setText("");
        storeEditText.setText("");
        brandEditText.setText("");
        genderEditText.setText("");
        seasonEditText.setText("");
        setupAdapters();
    }

    @OnClick(R.id.tv_delete)
    void onDeleteClicked() {
        presenter.onDeleteClicked();
    }
}