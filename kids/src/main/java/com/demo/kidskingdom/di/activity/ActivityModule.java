package com.demo.kidskingdom.di.activity;

import android.arch.lifecycle.Lifecycle;
import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.demo.kidskingdom.ui.filter.FilterScreen;
import com.rxmuhammadyoussef.core.component.activity.BaseActivity;
import com.rxmuhammadyoussef.core.di.qualifier.ForActivity;
import com.rxmuhammadyoussef.core.di.scope.ActivityScope;
import com.rxmuhammadyoussef.core.util.UiUtil;
import com.rxmuhammadyoussef.core.util.permission.PermissionUtil;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * This class is responsible for providing the requested objects to {@link ActivityScope} annotated classes
 */

@Module
public class
ActivityModule {

    private final BaseActivity activity;

    public ActivityModule(BaseActivity activity) {
        this.activity = activity;
    }

    @ActivityScope
    @Provides
    BaseActivity provideActivity() {
        return activity;
    }

    @ActivityScope
    @Provides
    FragmentManager provideFragmentManager() {
        return activity.getSupportFragmentManager();
    }

    @ActivityScope
    @Provides
    @ForActivity
    Context provideActivityContext() {
        return activity;
    }

    @ActivityScope
    @ForActivity
    @Provides
    Lifecycle provideLifCycle() {
        return activity.getLifecycle();
    }

    @ActivityScope
    @Provides
    UiUtil providesUiUtil() {
        return new UiUtil(activity);
    }

    @ActivityScope
    @ForActivity
    @Provides
    PermissionUtil providePermissionUtil() {
        return activity.getPermissionUtil();
    }

    @ActivityScope
    @ForActivity
    @Provides
    CompositeDisposable providesCompositeDisposable() {
        return new CompositeDisposable();
    }

    @ActivityScope
    @Provides
    FilterScreen provideFilterScreen() {
        return (FilterScreen) activity;
    }
}