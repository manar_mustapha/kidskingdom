package com.demo.kidskingdom.di.activity;

import com.demo.kidskingdom.di.fragment.FragmentComponent;
import com.demo.kidskingdom.di.fragment.FragmentModule;
import com.demo.kidskingdom.ui.filter.FilterActivity;
import com.demo.kidskingdom.ui.splash.SplashActivity;
import com.rxmuhammadyoussef.core.di.scope.ActivityScope;

import dagger.Subcomponent;

/**
 * This interface is used by dagger to generate the code that defines the connection between the provider of objects
 * (i.e. {@link ActivityModule}), and the object which expresses a dependency.
 */

@ActivityScope
@Subcomponent(modules = {ActivityModule.class})
public interface ActivityComponent {

    FragmentComponent plus(FragmentModule fragmentModule);

    void inject(SplashActivity splashActivity);

    void inject(FilterActivity filterActivity);

}